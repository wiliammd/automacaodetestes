describe "POST /signup" do
  context "Novo Usuario" do
    before(:all) do
      payload = { name: "Pitty", email: "pitty@bol.com.br", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])
      @result = Signup.new.create(payload)
      #puts @result.parsed_response
    end
    it "valida status code" do
      expect(@result.code).to eql 200
    end
    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  context "Usuario ja existe" do
    before(:all) do
      payload = { name: "João da Silva", email: "teste222@teste.com", password: "teste" }
      MongoDB.new.remove_user(payload[:email])
      Signup.new.create(payload)
      @result = Signup.new.create(payload)
    end
    it "valida status code" do
      expect(@result.code).to eql 409
    end
    it "deve retornar msg" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
  end

  exemples = Helpers::get_fixture("signup")

  exemples.each do |e|
    context e[:requisicao] do
      before(:all) do
        @result = Signup.new.create(e[:payload])
      end
      it "valida status code" do
        expect(@result.code).to eql e[:code]
      end
      it "valida id do usuário #{e[:code]}" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
