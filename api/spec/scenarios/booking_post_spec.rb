describe "POST/equipos/{equipo_id}/bookings" do
  before(:all) do
    payload = { email: "to@mate.com", password: "123" }
    result = Sessions.new.login(payload)
    @tomate_id = result.parsed_response["_id"]
  end

  context "Solicitar locacao" do
    before(:all) do
      #dado que Jose tem um equipamento para locacao = fender strato
      result = Sessions.new.login({ email: "jose@hotmail.com", password: "jose" })
      jose_id = result.parsed_response["_id"]
      fender = {
        thumbnail: Helpers::get_thumb("fender-sb.jpg"),
        name: "Fender strato",
        category: "Cordas",
        preco: 155,
      }
      MongoDB.new.remove_equipo(fender[:name], jose_id)

      result = Equipos.new.create(fender, jose_id)
      fender_id = result.parsed_response["_id"]

      #quando solicito a locação da fender do Jose
      @result = Equipos.new.booking(fender_id, @tomate_id)
    end
    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end
  end
end
