#DRY Don't Repeat Yourself => Não se Repita

describe "POST /sessions" do
  context "Login com sucesso" do
    before(:all) do
      payload = { email: "aiai@hotmail.com", password: "123" }
      @result = Sessions.new.login(payload)
    end
    it "valida status code" do
      expect(@result.code).to eql 200
    end
    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  exemples = Helpers::get_fixture("login")

  exemples.each do |e|
    context e[:requisicao] do
      before(:all) do
        @result = Sessions.new.login(e[:payload])
      end
      it "valida status code" do
        expect(@result.code).to eql e[:code]
      end
      it "valida id do usuário #{e[:code]}" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end

#   exemples = [
#     {
#       requisicao: "Login com senha invalida",
#       payload: { email: "teste@teste.com", password: "123" },
#       code: 401,
#       error: "Unauthorized",
#     },
#     {
#       requisicao: "Login Email invalido",
#       payload: { email: "teste401@teste.com", password: "123" },
#       code: 401,
#       error: "Unauthorized",
#     },
#     {
#       requisicao: "Login sem Email",
#       payload: { email: "", password: "123" },
#       code: 412,
#       error: "required email",
#     },
#     {
#       requisicao: "requisicao sem senha",
#       payload: { email: "teste@teste.com", password: "" },
#       code: 412,
#       error: "required password",
#     },
#     {
#       requisicao: "Login sem o campo email",
#       payload: { password: "123" },
#       code: 412,
#       error: "required email",
#     },
#     {
#       requisicao: "Login sem o campo senha",
#       payload: { email: "teste@teste.com" },
#       code: 412,
#       error: "required password",
#     },

#   ]
