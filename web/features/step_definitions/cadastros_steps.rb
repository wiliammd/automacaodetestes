Dado("que acesso a pagina de cadastro") do
  @signup_page.open
end

Quando("submeto o seguinte formulário de cadastro:") do |table|
  #log table.hashes tabelas com multiplos argumentos
  user = table.hashes.first

  MongoDB.new.remove_user(user[:email])
  @signup_page.create(user)
end
