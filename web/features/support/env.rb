require "allure-cucumber"
require "capybara"
require "capybara/cucumber"
require "faker"
require "mongo"

CONFIG = YAML.load_file(File.join(Dir.pwd, "features/support/config/#{ENV["CONFIG"]}"))
BROWSER = ENV["BROWSER"]

case ENV["BROWSER"]
when "chrome"
  @driver = :selenium_chrome
when "chrome_headless"
  Capybara.register_driver :selenium_chrome_headless do |app|
    Capybara::Selenium::Driver.load_selenium
    browser_options = ::Selenium::WebDriver::Chrome::Options.new.tap do |opts|
      opts.args << "--headless"
      opts.args << "--disable-gpu"
      opts.args << "--disable-site-isolation-trials"
      opts.args << "--no-sandbox"
      opts.args << "--disable-dev-shm-usage"
    end
    Capybara::Selenium::Driver.new(app, browser: :chrome, options: browser_options)
  end
  @driver = :selenium_chrome_headless
when "firefox"
  @driver = :selenium
when "fire_headless"
  @driver = :selenium_headless
else
  raise "Navegador incorreto :(, @drive null, selecione uma configuração em cucumber.yml com -p BROWSER??"
end

Capybara.configure do |config|
  config.default_driver = @driver
  config.default_max_wait_time = 15
  config.app_host = CONFIG["url"]
end

AllureCucumber.configure do |config|
  config.results_directory = "/logs"
  config.clean_results_directory = true
end
After do
  temp_shot = page.save_screenshot("logs/temp_screenshot.png")

  Allure.add_attachment(
    name: "Screenshot",
    type: Allure::ContentType::PNG,
    source: File.open(temp_shot),
  )
end
