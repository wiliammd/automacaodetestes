#language: pt
Funcionalidade: Cadastro de Anúncios
    Sendo usuário cadastro no ROcklov que possui equipamentos musicais
    Quero cadastar meus equipamentos
    Para que eu possa disponibiliza-los para locação

    Contexto: Login
        * Login com "testeweb@teste.com" e "teste"

    Cenario: Novo equipamento

        Dado que  acesso o formulario de cadastro de anúncios
            E que eu tenho o seguinte equipamento:
            | thumb     | fender-sb.jpg |
            | nome      | Fender Strato |
            | categoria | Cordas        |
            | preco     | 200           |
        Quando submeto o cadastro desse item
        Então devo ver esse item no meu Dashboard

    @teste
    Esquema do Cenario: Tentativa de cadastro de anúncios

        Dado que  acesso o formulario de cadastro de anúncios
            E que eu tenho o seguinte equipamento:
            | thumb     | <foto>      |
            | nome      | <nome>      |
            | categoria | <categoria> |
            | preco     | <preco>     |
        Quando submeto o cadastro desse item
        Então deve conter a mensagem de alerta: "<saida>"

        Exemplos:
            | foto          | nome            | categoria | preco | saida                                |
            |               | violao de nylon | Cordas    | 150   | Adicione uma foto no seu anúncio!    |
            | clarinete.jpg |                 | Outros    | 150   | Informe a descrição do anúncio!      |
            | mic.jpg       | microfone shure |           | 150   | Informe a categoria                  |
            | trompete.jpg  | trompete        | Outros    |       | Informe o valor da diária            |
            | conga.jpg     | conga           | Outros    | abc   | O valor da diária deve ser numérico! |
            | amp.jpg       | ampliicador     | Outros    | 100c  | O valor da diária deve ser numérico! |
