#language: pt
Funcionalidade:Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais
    @temporaria
    Cenario: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "testeweb@teste.com" e "teste"
        Então sou redirecionado para o Dashboard
    @vailogo
    Esquema do Cenario: Tentar logar
        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
        |email_input    |senha_input|mensagem_output                 |
        |testeweb@teste.com|teste12    |Usuário e/ou senha inválidos.   |
        |teste@40444.com|teste12    |Usuário e/ou senha inválidos.   |
        |teste&40444.com|teste12    |Oops. Informe um email válido!  |
        |               |teste12    |Oops. Informe um email válido!  |
        |teste@teste.com|           |Oops. Informe sua senha secreta!|


