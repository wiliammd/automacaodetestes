#language: pt

Funcionalidade: Cadastro
    Sendo musico que possui equipamentos musicais
    Quero fazer o meu cadastro no Rocklov
    Para que eu possa disponibilizá-los para locação

    @cadastro
    Cenario: Fazer meu Cadastro
        Dado que acesso a pagina de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome           | email              | senha    |
            |teste22| teste22@hotmail.com | senha123 |
        Então sou redirecionado para o Dashboard

    Esquema do Cenario:Tentativa de Cadastro
        Dado que acesso a pagina de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome         | email         | senha         |
            | <nome_input> | <email_input> | <senha_input> |
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | nome_input | email_input        | senha_input | mensagem_output                  |
            |            | wiliam@hotmail.com | senha123    | Oops. Informe seu nome completo! |
            | Wiliam     |                    | senha123    | Oops. Informe um email válido!   |
            | Wiliam     | wiliam*hotmail.com | senha123    | Oops. Informe um email válido!   |
            | Wiliam     | wiliam&hotmail.com | senha123    | Oops. Informe um email válido!   |
            | Wiliam     | wiliam@hotmail.com |             | Oops. Informe sua senha secreta! |





